﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimationStart : MonoBehaviour {

    public Animation anim;

	// Use this for initialization
	void Start () {
        anim.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
