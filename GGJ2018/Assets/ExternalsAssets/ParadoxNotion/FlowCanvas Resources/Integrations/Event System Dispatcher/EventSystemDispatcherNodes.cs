﻿using ParadoxNotion.Design;
using com.ootii.Messages;

namespace FlowCanvas.Nodes.EventDispacher{

	[Category("Events/Script")]
	[Description("Listens for events dispatched with ootii's Event System Dispatcher.")]
	public class OotiiMessageEvent : EventNode {

		[RequiredField]
		public string messageName;
		
		private FlowOutput fOut;
		private object data;
		private object sender;

		public override string name{
			get {return string.Format("{0} [ <color=#DDDDDD>{1}</color> ]", base.name, messageName);}
		}

		public override void OnGraphStarted(){
			base.OnGraphStarted();
			MessageDispatcher.AddListener(messageName, MessageReceived);
		}

		public override void OnGraphStoped(){
			base.OnGraphStoped();
			MessageDispatcher.RemoveListener(messageName, MessageReceived);
		}

		void MessageReceived(IMessage message){
			data = message.Data;
			sender = message.Sender;
			fOut.Call(new Flow());
		}

		protected override void RegisterPorts(){
			fOut = AddFlowOutput("Received");
			AddValueOutput("Message Sender", ()=> { return sender; });
			AddValueOutput("Message Data", ()=>{ return data; });
		}
	}



	[Category("Functions/Extensions/ootii Event Dispatcher")]
	public class DispatchOotiiMessage : CallableActionNode<object, string, object, float>{
		public override void Invoke(object sender, string name, object data, float delay){	
			MessageDispatcher.SendMessage(sender, name, data, delay);
		}
	}
}