﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using ParadoxNotion.Design;

namespace FlowCanvas.Nodes.EasySave{

	///UTILITY

	[Category("Extensions/EasySave/Utility")]
	public class Exists : PureFunctionNode<bool, string>{
		public override bool Invoke(string path){
			return ES2.Exists(path);
		}
	}

	[Category("Extensions/EasySave/Utility")]
	public class Rename : CallableActionNode<string, string>{
		public override void Invoke(string path, string newPath){
			ES2.Rename(path, newPath);
		}
	}

	[Category("Extensions/EasySave/Utility")]
	public class Delete : CallableActionNode<string>{
		public override void Invoke(string path){
			ES2.Delete(path);
		}
	}

	/////


	///WEB

	[Category("Extensions/EasySave/Web")]
	public class Upload<T> : LatentActionNode<T, string>{
		public override IEnumerator Invoke(T data, string url){
			var web = new ES2Web(url);
			yield return web.Upload(data);
			if (web.isError){
				Debug.LogError(web.errorCode + ":" + web.error);
			}
		}
	}

	[Category("Extensions/EasySave/Web")]
	public class Download<T> : LatentActionNode<string, string>{
		
		public T loadedValue{ get; private set; }

		public override IEnumerator Invoke(string url, string tag){
			var web = new ES2Web(url);
			yield return web.Download();
			if (web.isError){
				Debug.LogError(web.errorCode + ":" + web.error);
			}

			loadedValue = web.Load<T>(tag);
		}
	}

	[Category("Extensions/EasySave/Web")]
	public class DownloadList<T> : LatentActionNode<string, string>{
		
		public List<T> loadedValue{ get; private set; }

		public override IEnumerator Invoke(string url, string tag){
			var web = new ES2Web(url);
			yield return web.Download();
			if (web.isError){
				Debug.LogError(web.errorCode + ":" + web.error);
			}
			
			loadedValue = web.LoadList<T>(tag);
		}
	}

	///


	///SAVING

	[Category("Extensions/EasySave/Saving")]
	public class EasySave<T> : CallableActionNode<T, string>{
		public override void Invoke(T data, string path){
			ES2.Save(data, path);
		}
	}

	[Category("Extensions/EasySave/Saving")]
	public class EasySaveImage : CallableActionNode<Texture2D, string>{
		public override void Invoke(Texture2D image, string path){
			ES2.SaveImage(image, path);
		}
	}

	//LOADING

	[Category("Extensions/EasySave/Loading")]
	public class EasyLoad<T> : CallableFunctionNode<T, string>{
		public override T Invoke(string path){
			return ES2.Load<T>(path);
		}
	}
/*
	[Category("Extensions/EasySave/Loading")]
	public class EasyLoadComponent<T> : CallableFunctionNode<T, T, string> where T:Component{
		public override T Invoke(T component, string path){
			return ES2.Load<T>(path, component);
		}
	}
*/
	[Category("Extensions/EasySave/Loading")]
	public class EasyLoadList<T> : CallableFunctionNode<List<T>, string>{
		public override List<T> Invoke(string path){
			return ES2.LoadList<T>(path);
		}
	}

	[Category("Extensions/EasySave/Loading")]
	public class EasyLoadAudio : CallableFunctionNode<AudioClip, string>{
		public override AudioClip Invoke(string path){
			return ES2.LoadAudio(path);
		}
	}

	[Category("Extensions/EasySave/Saving")]
	public class EasyLoadImage : CallableFunctionNode<Texture2D, string>{
		public override Texture2D Invoke(string path){
			return ES2.LoadImage(path);
		}
	}
}