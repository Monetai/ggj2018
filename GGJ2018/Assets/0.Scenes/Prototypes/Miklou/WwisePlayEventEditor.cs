﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwisePlayEventEditor : MonoBehaviour {

    public string eventName;

	// Use this for initialization
	void Start () {
        AkSoundEngine.PostEvent(eventName, gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
