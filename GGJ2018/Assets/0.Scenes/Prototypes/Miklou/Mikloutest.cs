﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class Mikloutest : MonoBehaviour
    {
        public GameObject listener;

        private void Awake()
        {

            if(WwiseWrapper.Instance.fixdemerdepourlesondemaxime == false)
            {
                WwiseWrapper.WIFLoadBank(WwiseConstsBanks.SHARED);
                AkSoundEngine.PostEvent("music_launch", listener);
                AkSoundEngine.PostEvent("amb_space_tone", listener);
                WwiseWrapper.Instance.fixdemerdepourlesondemaxime = true;
            }

            AkSoundEngine.SetState("music", "music_step_menu");
        }
    }
}