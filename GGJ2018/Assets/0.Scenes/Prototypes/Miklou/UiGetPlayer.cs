﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class UiGetPlayer : MonoBehaviour {

    public TextMeshProUGUI textForPVPlayer01, textForPVPlayer02, textForPVPlayer03, textForPVPlayer04;
    public Life lifePlayer01, lifePlayer02, lifePlayer03, lifePlayer04;

    private void Start()
    {
        foreach(UnityEngine.UI.Image image in GetComponentsInChildren<UnityEngine.UI.Image>())
        {
            Color col = image.color;
            col.a = 0;
            image.color = col;
            image.DOFade(1, 2f);
        }

        foreach (UnityEngine.UI.Text text in GetComponentsInChildren<UnityEngine.UI.Text>())
        {
            Color col = text.color;
            col.a = 0;
            text.color = col;
            text.DOFade(1, 2f);
        }
    }

    // Update is called once per frame
    void Update ()
    {

        if(lifePlayer01 == null)
        {
            textForPVPlayer01.text = "K.I.A";

        }
        else
        {
            textForPVPlayer01.text = lifePlayer01.lifeAmount.ToString() + " PV";
        }

        if (lifePlayer02 == null)
        {
            textForPVPlayer02.text = "K.I.A";

        }
        else
        {
            textForPVPlayer02.text = lifePlayer02.lifeAmount.ToString() + " PV";
        }

        if (lifePlayer03 == null)
        {
            textForPVPlayer03.text = "K.I.A";

        }
        else
        {
            textForPVPlayer03.text = lifePlayer03.lifeAmount.ToString() + " PV";
        }

        if (lifePlayer04 == null)
        {
            textForPVPlayer04.text = "K.I.A";

        }
        else
        {
            textForPVPlayer04.text = lifePlayer04.lifeAmount.ToString() + " PV";
        }

    }
}
