// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "MagicUVLight"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_MainTex("MainTex", 2D) = "white" {}
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_MainTexNormalMap("MainTexNormalMap", 2D) = "bump" {}
		_HidedTex("HidedTex", 2D) = "white" {}
		_GlowColor("GlowColor", Color) = (0,1,0.006896496,1)
		_GlowIntensity("GlowIntensity", Range( 0 , 50)) = 1
		_NoiseTex("NoiseTex", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha addshadow fullforwardshadows dithercrossfade vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float2 texcoord_0;
		};

		uniform sampler2D _MainTexNormalMap;
		uniform float4 _MainTexNormalMap_ST;
		uniform sampler2D _HidedTex;
		uniform float4 _HidedTex_ST;
		uniform float4 _GlowColor;
		uniform float _GlowIntensity;
		uniform sampler2D _NoiseTex;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _Metallic;
		uniform float _Smoothness;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MainTexNormalMap = i.uv_texcoord * _MainTexNormalMap_ST.xy + _MainTexNormalMap_ST.zw;
			o.Normal = UnpackNormal( tex2D( _MainTexNormalMap, uv_MainTexNormalMap ) );
			float2 uv_HidedTex = i.uv_texcoord * _HidedTex_ST.xy + _HidedTex_ST.zw;
			float mulTime50 = _Time.y * 0.2;
			float2 panner47 = ( i.texcoord_0 + mulTime50 * float2( 0.2,-0.2 ));
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			o.Albedo = ( ( ( tex2D( _HidedTex, uv_HidedTex ) * _GlowColor * _LightColor0.a ) * _GlowIntensity * tex2D( _NoiseTex, panner47 ) ) + tex2D( _MainTex, uv_MainTex ) ).rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13201
0;92;708;461;2860.1;35.65822;3.711792;True;False
Node;AmplifyShaderEditor.RangedFloatNode;53;-1445.632,1110.9;Float;False;Constant;_Float0;Float 0;8;0;0.2;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.Vector2Node;51;-1246.172,989.0991;Float;False;Constant;_Vector0;Vector 0;8;0;0.2,-0.2;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.LightColorNode;26;-1412.963,560.5593;Float;True;0;3;COLOR;FLOAT3;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;52;-1250.047,872.9965;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;50;-1227.06,1116.635;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.PannerNode;47;-913.1007,886.3641;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;0.2;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;8;-1124.278,154.6423;Float;True;Property;_HidedTex;HidedTex;4;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;25;-1022.772,368.1199;Float;False;Property;_GlowColor;GlowColor;5;0;0,1,0.006896496,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.BreakToComponentsNode;27;-1048.109,562.0976;Float;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;48;-486.6763,710.4005;Float;True;Property;_NoiseTex;NoiseTex;7;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-444.9167,167.5251;Float;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;37;-472.3984,457.205;Float;False;Property;_GlowIntensity;GlowIntensity;6;0;1;0;50;0;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;4;-877.4536,-568.6899;Float;True;Property;_MainTex;MainTex;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-9.423586,101.0327;Float;True;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;83;-2697.115,-348.8147;Float;False;Constant;_Float1;Float 1;9;0;50;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;89;-2023.093,-68.14392;Float;False;Constant;_Float3;Float 3;9;0;0.01;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;87;-1708.219,11.72466;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;88;-1984.797,-274.8395;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SinOpNode;78;-2133.764,-362.6407;Float;False;1;0;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.TimeNode;82;-2749.255,-270.7433;Float;False;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;80;-2335.915,-300.3268;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;86;-2145.118,-195.8147;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;54;11.14966,-276.1439;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;7;-861.1701,-38.22494;Float;False;Property;_Smoothness;Smoothness;2;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.DynamicAppendNode;72;-1523.86,231.6821;Float;False;FLOAT4;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.TextureCoordinatesNode;56;-2012.787,255.3475;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;6;-860.3113,-139.1598;Float;False;Property;_Metallic;Metallic;1;0;0;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;85;-2477.708,2.062405;Float;False;Constant;_Float2;Float 2;9;0;20;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.PosVertexDataNode;79;-2741.436,-67.52832;Float;False;0;0;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;2;-874.8105,-365.4297;Float;True;Property;_MainTexNormalMap;MainTexNormalMap;3;0;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-2329.411,-139.9264;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;313.5902,-150.8074;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;MagicUVLight;False;False;False;False;False;False;False;False;False;False;False;False;True;False;True;False;False;Back;0;0;False;0;0;Transparent;0.5;True;True;0;False;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;50;0;53;0
WireConnection;47;0;52;0
WireConnection;47;2;51;0
WireConnection;47;1;50;0
WireConnection;27;0;26;0
WireConnection;48;1;47;0
WireConnection;10;0;8;0
WireConnection;10;1;25;0
WireConnection;10;2;27;3
WireConnection;35;0;10;0
WireConnection;35;1;37;0
WireConnection;35;2;48;0
WireConnection;87;0;88;0
WireConnection;87;1;56;1
WireConnection;88;0;78;0
WireConnection;88;1;89;0
WireConnection;78;0;86;0
WireConnection;80;0;83;0
WireConnection;80;1;82;1
WireConnection;86;0;80;0
WireConnection;86;1;84;0
WireConnection;54;0;35;0
WireConnection;54;1;4;0
WireConnection;72;0;87;0
WireConnection;72;1;56;2
WireConnection;84;0;79;2
WireConnection;84;1;85;0
WireConnection;0;0;54;0
WireConnection;0;1;2;0
WireConnection;0;3;6;0
WireConnection;0;4;7;0
ASEEND*/
//CHKSM=DB4BDF6B367703055F8941DE3BE0550B37815223