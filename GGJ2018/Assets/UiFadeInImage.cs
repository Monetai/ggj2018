﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UiFadeInImage : MonoBehaviour {

    private void Start()
    {
        Color col = GetComponent<Image>().color;
        col.a = 1;
        GetComponent<Image>().color = col;
        GetComponent<Image>().DOFade(0, 2.5f);
    }
}
