/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB_SPACE_TONE = 3749922539U;
        static const AkUniqueID ENV_SHIP_ALARM = 2221367865U;
        static const AkUniqueID ENV_SPACE_SHIP_FIRE = 2354463077U;
        static const AkUniqueID MUSIC_LAUNCH = 4263986920U;
        static const AkUniqueID SFX_NMI_CHARGE = 1096874422U;
        static const AkUniqueID SFX_NMI_FALL_IMPACT = 4031224734U;
        static const AkUniqueID SFX_NMI_REA_DESTROY = 477901621U;
        static const AkUniqueID SFX_NMI_REA_IMPACT = 1938563783U;
        static const AkUniqueID SFX_NMI_SHOOT = 291514315U;
        static const AkUniqueID SFX_SHOTGUN_CANTSHOOT = 2359095743U;
        static const AkUniqueID SFX_SHOTGUN_SHOOT = 2668619497U;
        static const AkUniqueID SFX_UI_VALIDATION = 2836293135U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace STATE
            {
                static const AkUniqueID MUSIC_STEP_END = 1659211129U;
                static const AkUniqueID MUSIC_STEP_GAME = 715646748U;
                static const AkUniqueID MUSIC_STEP_GAME_BOSS_DOWN = 114097775U;
                static const AkUniqueID MUSIC_STEP_GAME_ONE_PLAYER = 1032362491U;
                static const AkUniqueID MUSIC_STEP_MENU = 2578102635U;
            } // namespace STATE
        } // namespace MUSIC

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID SC_MUSIC = 1834744151U;
        static const AkUniqueID SC_SFX = 4170244107U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID SHARED = 3574630834U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID COMMUNICATION = 530303819U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
