﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TitleShowFade : MonoBehaviour
{
    Image title;


	// Use this for initialization
	void Start () {
        Show();
    }

    public void Show()
    {
        title = GetComponentInChildren<Image>();

        title.rectTransform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        title.rectTransform.DOScale(1, 1.9f);
        Color col = title.color;
        col.a = 0;
        title.color = col;
        title.DOFade(1, 2f);
    }

    public void Hide()
    {
        title = GetComponentInChildren<Image>();

        title.rectTransform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        title.rectTransform.DOScale(0f, 1.9f);
        Color col = title.color;
        col.a = 1;
        title.color = col;
        title.DOFade(0, 2f);
    }

}
