﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    [CreateAssetMenu(menuName = "LittleWitch/ProgresionFlag/create new ProgressionFlag")]
    public class ProgressionFlag : ScriptableObject
    {

#if UNITY_EDITOR
        [Multiline]
        [ValidateInput("HasADescription", "No comment? Really?", InfoMessageType.Warning)]
        public string Comment = "";

        public bool HasADescription(string comment)
        {
            return !string.IsNullOrEmpty(comment);
        }
#endif

        [SerializeField]
        private string flagName = "";
        [SerializeField]
        private bool defaultFlagState = false;
        private bool currentFlagState;

        public bool CurrentFlagState
        {
            get { return currentFlagState; }
            set { currentFlagState = value; }
        }

        public string FlageName
        {
            get { return flagName; }
        }

        private void OnEnable()
        {
            currentFlagState = defaultFlagState;
        }
    }
}