﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class WaitAndRestInteraction : InteractionBrain
    {
        public string clip;

        WaitForSeconds wait3sec = new WaitForSeconds(3f);

        protected override void OnInteractionBegin(BaseInteraction interaction)
        {
            base.OnInteractionBegin(interaction);
            StartCoroutine(Wait());
        }

        IEnumerator Wait()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Animator>().Play(clip, 0);
            yield return wait3sec;
            CompleteInteraction();
        }
    }
}