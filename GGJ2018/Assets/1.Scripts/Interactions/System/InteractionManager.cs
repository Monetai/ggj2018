﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace LittleWitch
{
    public class InteractionManager : MonoBehaviour
    {
        public float interactionRadius = 1.5f;
        List<BaseInteraction> nearbyInteractions = new List<BaseInteraction>();
        private BaseInteraction currentInteraction;

        //put this in a coroutine
        public void Update()
        {
            FetchNearbyInteraction();

            if(currentInteraction != null)
                currentInteraction.UpdateInteraction();
        }

        //interaction fetch
        private void FetchNearbyInteraction()
        {
            nearbyInteractions.Clear();
            BaseInteraction[] interactions = null;
    
            //need to fetch all interaction here

            if (interactions != null)
            {
                for (int i = 0; i < interactions.Length; i++)
                {
                    if (Vector3.Dot(transform.forward, interactions[i].transform.position - transform.position) > 0)
                    {
                        if (Vector3.Distance(interactions[i].transform.position, transform.position) < interactionRadius)
                            nearbyInteractions.Add(interactions[i]);
                    }
                }
            }
        }

        public bool IsInteractionNearby()
        {
            FetchNearbyInteraction();
            return nearbyInteractions.Count > 0;
        }

        public bool CanInteract()
        {
            FetchNearbyInteraction();
            return nearbyInteractions.Count > 0 && nearbyInteractions[0].InteractionIsReady();
        }

        public BaseInteraction GetBestInteractionAvailable()
        {
			//classer les interactions par distance et dot product
            if (nearbyInteractions.Count == 0)
                return null;
            else
                return nearbyInteractions[0];
        }

        //interaction controls

        public bool IsInteracting()
        {
            if (currentInteraction == null)
                return false;

            if (currentInteraction.IsInteracting)
                return true;
            else
                return false;
        }

        public BaseInteraction GetCurrentInteraction()
        {
            return currentInteraction;
        }

        /// <summary>
        /// We want to interact with the best Interaction available
        /// </summary>
        public void PrepareInteraction()
        {
            currentInteraction = GetBestInteractionAvailable();
            if (currentInteraction && currentInteraction.InteractionIsReady())
            {
                currentInteraction.OnInteractionClosed += OnCurrentInteractionEnd;
                currentInteraction.TryBeginInteraction();
            }
            else
                currentInteraction = null;
        }

        /// <summary>
        /// We want to interact with an interaction
        /// </summary>
        /// <param name="interaction"></param>
        public void PrepareInteraction(BaseInteraction interaction)
        {
            currentInteraction = interaction;
            if (currentInteraction.TryBeginInteraction())
            {
                currentInteraction.OnInteractionClosed += OnCurrentInteractionEnd;
            }
            else
                currentInteraction = null;
        }

        public void ForceCancel()
        {
            currentInteraction.CancelInteraction();
        }

        //callbacks
        public void OnCurrentInteractionEnd(BaseInteraction interaction)
        {
            currentInteraction.OnInteractionClosed -= OnCurrentInteractionEnd;
            currentInteraction = null;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, interactionRadius);
        }
    }
}