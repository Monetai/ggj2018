﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    /// <summary>
    /// Provide a basic interface for interaction brain
    /// Allow to easily create complex interactions
    /// </summary>
    public abstract class InteractionBrain : MonoBehaviour
    {
        protected BaseInteraction owningInteraction;

        public virtual void SetInteraction(BaseInteraction interaction)
        {
            if (owningInteraction != null)
                UnregisterInteraction(owningInteraction);

            owningInteraction = interaction;

            if (owningInteraction != null)
                RegisterInteraction(owningInteraction);
        }

        public virtual void CompleteInteraction()
        {
            owningInteraction.CompleteInteraction();
        }
       
        public virtual void ResetBrain()
        {
        }

        /// <summary>
        /// Can the brain be stopped (the user want to cancel interaction)
        /// </summary>
        /// <returns></returns>
        public virtual bool CanBeStopped() {
            return true;
        }

		public virtual bool CanBeginInteraction() {
            return true;
		}

        protected void RegisterInteraction(BaseInteraction interaction)
        {
            interaction.OnInteractionBegin += OnInteractionBegin;
            interaction.OnInteractionCanceled += OnInteractionCanceled;
            interaction.OnInteractionDone += OnInteractionDone;
            interaction.OnInteractionUpdate += UpdateInteractionBrain;
            interaction.OnInteractionClosed += OnInteractionClosed;
        }

        protected void UnregisterInteraction(BaseInteraction interaction)
        {
            interaction.OnInteractionBegin -= OnInteractionBegin;
            interaction.OnInteractionCanceled -= OnInteractionCanceled;
            interaction.OnInteractionDone -= OnInteractionDone;
            interaction.OnInteractionUpdate -= UpdateInteractionBrain;
            interaction.OnInteractionClosed -= OnInteractionClosed;
        }

        protected virtual void UpdateInteractionBrain(BaseInteraction interaction)
        {
        }

        protected virtual void OnInteractionBegin(BaseInteraction interaction)
        {
        }

        protected virtual void OnInteractionCanceled(BaseInteraction interaction)
        {
        }

        protected virtual void OnInteractionDone(BaseInteraction interaction)
        {
        }

        /// <summary>
        /// Called when the interaction is either Done or Canceled
        /// </summary>
        protected virtual void OnInteractionClosed(BaseInteraction interaction)
        {
        }
    }
}