﻿using FlowCanvas;
using FlowCanvas.Nodes;
using ParadoxNotion.Design;
using System.Collections;
using UnityEngine.Serialization;
using System.Collections.Generic;
using UnityEngine;
using System;
using NodeCanvas;
using NodeCanvas.Framework;

namespace LittleWitch
{
	[Name("FlowBrain")]
	[Category("Events/Interaction")]
	[Description("Create a custom brain")]
	public class FlowBrain : EventNode
    {
		public BBParameter<FlowInteractionBrain> brain;

		[HideInInspector]
		public FlowInput inCancelInteraction;
		[HideInInspector]
		public FlowInput inCompleteInteraction;

		[HideInInspector]
		public ValueInput<bool> canBeStopped;

		[HideInInspector]
		public ValueInput<bool> canBeginInteraction;

        [HideInInspector]
        public FlowOutput outResetBrain;
        [HideInInspector]
		public FlowOutput outInteractionBegin;
		[HideInInspector]
		public FlowOutput outInteractionCanceled;
		[HideInInspector]
		public FlowOutput outInteractionDone;
		[HideInInspector]
		public FlowOutput outInteractionClosed;
		[HideInInspector]
		public FlowOutput outInteractionUpdated;

		protected override void RegisterPorts() {
			base.RegisterPorts();

			canBeStopped = AddValueInput<bool>("CanBeStopped");
			canBeStopped.serializedValue = true;
			canBeginInteraction = AddValueInput<bool>("CanBeginInteraction");
			canBeginInteraction.serializedValue = true;

			inCancelInteraction = AddFlowInput("CancelInteraction", (f) => {
				brain.value.CancelInteraction();
			});

			inCompleteInteraction = AddFlowInput("ValidateInteraction", (f) => {
                brain.value.CompleteInteraction();
			});

			outInteractionBegin = AddFlowOutput("OnInteractionBegin");
			outInteractionUpdated = AddFlowOutput("OnInteractionUpdate");
			outInteractionDone = AddFlowOutput("OnInteractionDone");
            outInteractionCanceled = AddFlowOutput("OnInteractionCanceled");
            outInteractionClosed = AddFlowOutput("OnInteractionClose");

            outResetBrain = AddFlowOutput("OnResetBrain");
		}

		public override void OnGraphStarted() {
			base.OnGraphStarted();

            if (brain.value == null)
            {
                brain.value = graphAgent.GetComponent<FlowInteractionBrain>();
            }

            RegisterToInteraction();
		}

		public override void OnGraphStoped() {
			base.OnGraphStoped();
			UnRegisterToInteraction();
		}

		public override void OnDestroy() {
			base.OnDestroy();

			UnRegisterToInteraction();
		}

		private void RegisterToInteraction() {
			if (brain.isNull)
				return;

			brain.value.node = this;
		}

		private void UnRegisterToInteraction() {
			if (brain.isNull)
				return;

			brain.value.node = null;
		}
	}

}
