﻿using FlowCanvas;
using ParadoxNotion.Design;
using System.Collections.Generic;

namespace RM.FlowNodes {
	[Name("FlowWarp")]
	[Category("Actions/FlowWarp")]
	[Description("Teleports the flow from one place to one other. Allow better visibility and help keeping the graph clean")]
	public class FlowWarp : FlowNode {
		public enum WarpType { ENTER, EXIT }

		public WarpType _warpType = WarpType.ENTER;

		#region InputOutput
		private FlowInput _in;
		private FlowOutput _out;
		#endregion

		#region Memory 
		private WarpType _previousType = WarpType.ENTER;
		#endregion

		private static Dictionary<int, List<FlowWarp>> _warps = new Dictionary<int, List<FlowWarp>>();

		protected override void RegisterPorts() {
			base.RegisterPorts();

			RegisterWarp();

			if(_warpType == WarpType.ENTER) {
				_in = AddFlowInput("", (f) => {
					ExecuteAction(f);
				});
			} else {
				// its an exit, only provide out
				_out = AddFlowOutput("");
			}
		}

#if UNITY_EDITOR
		protected override void OnNodeInspectorGUI() {
			base.OnNodeInspectorGUI();
			
			if(_previousType != _warpType) {
				GatherPorts();
			}

			_previousType = _warpType;
		}
#endif

		private void ExecuteAction(Flow f) {
			List<FlowWarp> graphWarps;
			int instanceID = graph.GetInstanceID();

			if (!_warps.ContainsKey(instanceID)) {
				return;
			}

			graphWarps = _warps[instanceID];

			for (int i = graphWarps.Count - 1; i >= 0f; i--) {
				FlowWarp warp = graphWarps[i];

				if (warp == null) {
					graphWarps.RemoveAt(i);
					continue;
				}

				if(warp._warpType == WarpType.EXIT) {
					warp.OnFlowRecieved(this, f);
				}
			}
		}

		private void RegisterWarp() {
			List<FlowWarp> graphWarps;
			int instanceID = graph.GetInstanceID();

			if (!_warps.ContainsKey(instanceID)) {
				_warps.Add(instanceID, new List<FlowWarp>());
			}

			graphWarps = _warps[instanceID];

			if(!graphWarps.Contains(this)) {
				graphWarps.Add(this);
			}
		}

		public void OnFlowRecieved(FlowWarp origin, Flow f) {
			_out.Call(f);
		}
	}
} 