﻿using FlowCanvas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
	public class FlowInteractionBrain : InteractionBrain
    {
		public FlowBrain node;

		public override void ResetBrain() {
			base.ResetBrain();

			if (node == null)
				return;
			
			node.outResetBrain.Call(new Flow());
		}

		public override bool CanBeStopped() {
			return base.CanBeStopped() && (node != null ? node.canBeStopped.value : true);
		}

		public override bool CanBeginInteraction() {
			return base.CanBeginInteraction() && (node != null ? node.canBeginInteraction.value : true);
		}

		protected override void OnInteractionBegin(BaseInteraction interaction) {
			base.OnInteractionBegin(interaction);

			if (node == null)
				return;

			node.outInteractionBegin.Call(new Flow());
		}

		protected override void OnInteractionCanceled(BaseInteraction interaction) {
			base.OnInteractionCanceled(interaction);

			if (node == null)
				return;

			node.outInteractionCanceled.Call(new Flow());
		}

		protected override void OnInteractionDone(BaseInteraction interaction) {
			base.OnInteractionDone(interaction);

			if (node == null)
				return;

			node.outInteractionDone.Call(new Flow());
		}

		protected override void OnInteractionClosed(BaseInteraction interaction) {
			base.OnInteractionClosed(interaction);

			if (node == null)
				return;

			node.outInteractionClosed.Call(new Flow());
		}

		protected override void UpdateInteractionBrain(BaseInteraction interaction) {
            base.UpdateInteractionBrain(interaction);
			if (node == null)
				return;

			node.outInteractionUpdated.Call(new Flow());
		}

		public void CancelInteraction() {
            owningInteraction.CancelInteraction();
		}

        public override void CompleteInteraction() {
            base.CompleteInteraction();
		}
	}
}