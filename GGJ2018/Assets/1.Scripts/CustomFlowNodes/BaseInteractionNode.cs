﻿using FlowCanvas;
using FlowCanvas.Nodes;
using ParadoxNotion.Design;
using System.Collections;
using UnityEngine.Serialization;
using System.Collections.Generic;
using UnityEngine;
using System;
using NodeCanvas;
using NodeCanvas.Framework;

namespace LittleWitch
{
	[Name("BaseInteractionNode")]
	[Category("Events/Interaction/BaseInteractionNode")]
	[Description("Represent a base interaction")]
	public class BaseInteractionNode : EventNode {
		public BBParameter<BaseInteraction> owningInteraction;

		private FlowOutput outInteractionBegin;
		private FlowOutput outInteractionCanceled;
		private FlowOutput outInteractionDone;
        private FlowOutput outInteractionClosed;
        private FlowOutput outInteractionUpdated;

        protected override void RegisterPorts() {
			base.RegisterPorts();

			outInteractionBegin = AddFlowOutput("OnInteractionBegin");
			outInteractionCanceled = AddFlowOutput("OnInteractionCanceled");
			outInteractionDone = AddFlowOutput("OnInteractionDone");
            outInteractionClosed = AddFlowOutput("OnInteractionClosed");
            outInteractionUpdated = AddFlowOutput("OnInteractionUpdated");
        }

        public override void OnGraphStarted() {
			base.OnGraphStarted();
			if (owningInteraction.value == null)
				owningInteraction.value = graphAgent.GetComponent<BaseInteraction>();
			RegisterToInteraction();
		}

		public override void OnGraphStoped() {
			base.OnGraphStoped();

			UnRegisterToInteraction();
		}

		private void RegisterToInteraction() {
			if (owningInteraction.isNull)
				return;

			owningInteraction.value.OnInteractionBegin += OnInteractionBegin;
			owningInteraction.value.OnInteractionCanceled += OnInteractionCanceled;
			owningInteraction.value.OnInteractionDone += OnInteractionDone;
            owningInteraction.value.OnInteractionUpdate += OnInteractionUpdated;
            owningInteraction.value.OnInteractionClosed += OnInteractionClosed;
        }

		private void UnRegisterToInteraction() {
			if (owningInteraction.isNull)
				return;

			owningInteraction.value.OnInteractionBegin -= OnInteractionBegin;
			owningInteraction.value.OnInteractionCanceled -= OnInteractionCanceled;
			owningInteraction.value.OnInteractionDone -= OnInteractionDone;
            owningInteraction.value.OnInteractionUpdate -= OnInteractionUpdated;
            owningInteraction.value.OnInteractionClosed -= OnInteractionClosed;
        }


		#region Callbacks
		private void OnInteractionBegin(BaseInteraction interaction) {
			outInteractionBegin.Call(new Flow());
		}

		private void OnInteractionCanceled(BaseInteraction interaction) {
			outInteractionCanceled.Call(new Flow());
		}

		private void OnInteractionDone(BaseInteraction interaction) {
			outInteractionDone.Call(new Flow());
		}

		private void OnInteractionUpdated(BaseInteraction interaction) {
            outInteractionUpdated.Call(new Flow());
        }

        private void OnInteractionClosed(BaseInteraction interaction) {
            outInteractionClosed.Call(new Flow());
        }

        #endregion
    }

}
