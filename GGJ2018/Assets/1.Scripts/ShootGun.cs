﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShootGun : MonoBehaviour {

    public Action OnShoot;
    public Action OnCantShoot;

    public float radius;
    public float distance;
    public int dammageAmount;

    public float cooldown = 1.5f;
    private bool isCooling = false;

    // Use this for initialization
    public void Shoot()
    {
        if (isCooling)
        {
            if (OnCantShoot != null)
                OnCantShoot.Invoke();

            return;
        }

        if (OnShoot != null)
            OnShoot.Invoke();

        foreach (RaycastHit hit in Physics.SphereCastAll(transform.position, radius, transform.forward, distance))
        {
            if(hit.collider.gameObject != transform.parent.gameObject && hit.collider.GetComponent<Life>() != null)
                hit.collider.GetComponent<Life>().TakeDammage(dammageAmount,hit.point);
        }

        if(isCooling == false)
            StartCoroutine(Cooling());
    }

    IEnumerator Cooling()
    {
        isCooling = true;
        yield return new WaitForSeconds(cooldown);
        isCooling = false;
    }

    public bool IsReadyToShoot()
    {
        return !isCooling;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, radius);
        Gizmos.DrawRay(transform.position, transform.forward * distance);
    }
}
