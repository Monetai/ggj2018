﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Diagnostics;
using System.IO;

public class FractureCaller : MonoBehaviour {

    private static void CallBlenderFragmentation()
    {
        UnityEngine.Debug.Log("----Custom Script---Je lance blender.");
        Process.Start(Application.dataPath + "/blenderFragment.bat");
        UnityEngine.Debug.Log("----Custom Script---J'ai lancé blender!!");
    }

    [MenuItem("Assets/FragmentMesh")]
    private static void DoSomethingWithTexture()
    {
        CallBlenderFragmentation();
    }

    // Note that we pass the same path, and also pass "true" to the second argument.
    [MenuItem("Assets/FragmentMesh", true)]
    private static bool NewMenuOptionValidation()
    {
        // This returns true when the selected object is a Texture2D (the menu item will be disabled otherwise).
        return Path.GetExtension(AssetDatabase.GetAssetPath(Selection.activeObject)) == ".obj" || Path.GetExtension(AssetDatabase.GetAssetPath(Selection.activeObject)) == ".fbx";
    }
}

