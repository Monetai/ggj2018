﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LittleWitch
{
    [CustomEditor(typeof(StateController))]
    [CanEditMultipleObjects]
    public class StateControllerInspector : Editor
    {
        private StateController controller;
        private UnityEditorInternal.ReorderableList actionList;

        private State currentlySelectedAction;

        void OnEnable()
        {
            controller = target as StateController;

            controller.Init();

            actionList = new UnityEditorInternal.ReorderableList(controller.States,
                typeof(State),
                false, false, false, false);

            actionList.drawElementCallback += DisplayActionField;
            actionList.onRemoveCallback += OnRemoveCallback;
        }

        public override void OnInspectorGUI()
        {
            if (actionList == null)
                return;

            if (controller.States == null || controller.States.Count <= 0)
            {
                GUI.color = Color.red;
                GUILayout.Label("Please add a State to continue");

                return;
            }

            actionList.DoLayoutList();
            DestroyAction();
        }

        private void DisplayActionField(Rect rect, int index, bool active, bool focused)
        {
            State action = controller.States[index];//actionList.serializedProperty.GetArrayElementAtIndex(index);

            rect.y += 2;

            if (action == null)
                return;

            EditorGUI.LabelField(new Rect(rect.x, rect.y, 100, EditorGUIUtility.singleLineHeight),
                action.GetType().ToString());

            Color baseColor = GUI.color;

            if (action.IsEnabled)
            {
                if (Application.isPlaying)
                {
                    if (action == controller.CurrentState)
                    {
                        GUI.color = Color.green * 0.9f;
                    }
                    else
                    {
                        GUI.color = (action.CanBeActive()) ? Color.yellow * 0.9f : Color.red * 0.9f;
                    }
                }
                else
                    GUI.color = Color.green * 0.9f;
            }
            else
            {
                GUI.color = Color.grey;
            }

            action.id = EditorGUI.TextField(new Rect(rect.x + 100, rect.y, rect.width - 100 - 35, EditorGUIUtility.singleLineHeight),
                action.id);

            GUI.color = baseColor;

            action.Priority = EditorGUI.DelayedIntField(new Rect(rect.x + rect.width - 35 + 2, rect.y, 35 - 2, EditorGUIUtility.singleLineHeight),
                action.Priority);
        }

        private State actionToDelete = null;
        private void OnRemoveCallback(UnityEditorInternal.ReorderableList list)
        {
            Debug.Log("ActionControllerEditor : Removing action " + controller.States[actionList.index]);
            State a = controller.States[actionList.index];
            //ReorderableList.defaultBehaviours.DoRemoveButton(list);
            controller.States.RemoveAt(actionList.index);
            actionToDelete = a;
        }

        private void DestroyAction()
        {
            if (actionToDelete != null)
            {
                DestroyImmediate(actionToDelete);
                actionToDelete = null;
            }
        }
    }
}