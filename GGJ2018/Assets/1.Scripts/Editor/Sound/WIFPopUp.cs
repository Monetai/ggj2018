﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LittleWitch
{
    public class WIFPopUp : EditorWindow
    {

        List<WIFEvents> wIFEventAdded = new List<WIFEvents>();
        List<string> wIFEventDeleted = new List<string>();

        Vector2 scrollPosAdded = Vector2.zero;
        Vector2 scrollPosDeleted = Vector2.zero;

        void OnGUI()
        {

            EditorGUILayout.LabelField("---- WIF Event Added ----");
            scrollPosAdded = EditorGUILayout.BeginScrollView(scrollPosAdded);
            {
                foreach (WIFEvents wIFEvent in wIFEventAdded)
                {
                    EditorGUILayout.LabelField(wIFEvent.eventName);
                }
            }
            EditorGUILayout.EndScrollView();

            EditorGUILayout.LabelField("---- WIF Event Deleted ----");

            scrollPosDeleted = EditorGUILayout.BeginScrollView(scrollPosDeleted);
            {
                foreach (string s in wIFEventDeleted)
                {
                    EditorGUILayout.LabelField(s);
                }
            }
            EditorGUILayout.EndScrollView();


            if (GUILayout.Button("Close"))
                Close();

            Event e = Event.current;
            switch (e.type)
            {
                case EventType.KeyDown:
                    {
                        if (Event.current.keyCode == (KeyCode.Return))
                        {
                            Close();
                        }
                        break;
                    }
            }
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        public static void ShowWIFEventMajPopUp(List<WIFEvents> wIFEventAdded, List<string> wIFEventDeleted)
        {
            WIFPopUp window = (WIFPopUp)ScriptableObject.CreateInstance("WIFPopUp");

            if (wIFEventAdded.Count > 0 || wIFEventDeleted.Count > 0)
            {
                window.wIFEventAdded = wIFEventAdded;
                window.wIFEventDeleted = wIFEventDeleted;
                window.ShowUtility();
            }
        }
    }
}