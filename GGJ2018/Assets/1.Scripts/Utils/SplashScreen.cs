﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

    public float loadNextSceneIn = 3f; 

	// Use this for initialization
	void Start () {
        Invoke("LoadNextScene",loadNextSceneIn);

    }
	
	// Update is called once per frame
	void LoadNextScene () {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
}
