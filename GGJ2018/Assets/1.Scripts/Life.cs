﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Life : MonoBehaviour
{
    public Action OnDeath;
    public Action<Vector3> OnTakeDammage;

    public int lifeAmount = 5;
    public bool canTakeDammage = true;

	public void TakeDammage(int dammageAmount, Vector3 hitPosition)
    {
        if (canTakeDammage == false)
            return;

        lifeAmount -= dammageAmount;

        if (lifeAmount <= 0)
        {
            if(OnDeath != null)
                OnDeath.Invoke();

            lifeAmount = 0;
        }
        else
        {
            if (OnTakeDammage != null)
                OnTakeDammage.Invoke(hitPosition);
        }
    }
}
