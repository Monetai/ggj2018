﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Life))]
public class LifeEventsParticle : MonoBehaviour
{
    Life life;

    public GameObject objectToInstantiateOnTakeDammage;
    public GameObject objectToInstantiateOnDeath;

    // Use this for initialization
    void Start () {
        life = GetComponent<Life>();

        life.OnDeath += OnDeath;
        life.OnTakeDammage += OnTakeDammage;
    }

    void OnTakeDammage(Vector3 hitPosition)
    {
        AkSoundEngine.PostEvent("sfx_nmi_rea_impact", objectToInstantiateOnTakeDammage);
        GameObject obj = Instantiate(objectToInstantiateOnTakeDammage);
        obj.transform.position = hitPosition;
    }

    void OnDeath()
    {
        AkSoundEngine.PostEvent("sfx_nmi_rea_destroy", objectToInstantiateOnDeath);
        GameObject obj = Instantiate(objectToInstantiateOnDeath);
        obj.transform.position = life.transform.position;
        Destroy(gameObject);
    }
}
