﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class Hurt : State
    {
        [BoxGroup("Mandatory References")]
        public Animator animator;
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;
        public Life life;


        protected override void Awake()
        {
            base.Awake();
        }

        #region StateCallbacks
        public override bool CanBeActive()
        {
            return controller.GetDictionnaryValue("isHurt") == "true";
        }

        public override void OnUpdateState()
        {
            base.OnUpdateState();
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            animator.SetBool("isHurt", false);
        }

        bool isHurt;
        public override bool CanBeSkipped()
        {
            return isHurt == false;
        }

        public override void OnBecameCurrentState(State previousState)
        {
            //animator.SetFloat(speedId, 0);
            base.OnBecameCurrentState(previousState);

            animator.SetBool("isHurt", true);

            StartCoroutine(Wait());
        }

        IEnumerator Wait()
        {
            InputProvider.Instance.Vibrate(1f, 0.5f, inputManager.GetCurrentInputSource().GetPlayerIndex());
            isHurt = true;
            life.canTakeDammage = false;
            yield return new WaitForSeconds(1.5f);
            life.canTakeDammage = true;
            isHurt = false;
            controller.SetDictionnaryValue("isHurt","false");
        }
        #endregion
    }
}
