﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPhysicController : MonoBehaviour
{

    Rigidbody body;
    public LayerMask mask;

    public float groundedDrag = 3f;
    public float maxVelocity = 50;
    public float hoverForce = 1000;

    [Range(0, 1)]
    public float hoverDamping = 0.99f;
    public float gravityForce = 1000f;
    public float hoverHeight = 1.5f;
    public GameObject[] hoverPoints;
    public GameObject[] Wheels;
    public GameObject leftPushPoint;
    public GameObject rightPushPoint;

    public float forwardAcceleration = 8000f;
    public float reverseAcceleration = 4000f;
    float thrust = 0f;

    public float turnStrength = 1000f;
    float turnValue = 0f;

    int layerMask;


    Vector3 basePose;

    void Start()
    {
        basePose = transform.position;
        body = GetComponent<Rigidbody>();
        body.centerOfMass = Vector3.down;

        layerMask = mask;
    }
    public float acceleration = 0;
    public float turnAxis = 0;
    void Update()
    {
        // Main Thrust
        thrust = 0.0f;
        if (acceleration > 0.1f)
            thrust = acceleration * forwardAcceleration;
        else if (acceleration < -0.1f)
            thrust = acceleration * reverseAcceleration;

        // Turning
        turnValue = 0.0f;
        if (Mathf.Abs(turnAxis) > 0.1)
            turnValue = turnAxis;

    } 
    public Vector3 target;
    private void FixedUpdate()
    {
        RaycastHit hit;
        bool grounded = false;
        for (int i = 0; i < hoverPoints.Length; i++)
        {
            GameObject hoverPoint = hoverPoints[i];
            if (Physics.Raycast(hoverPoint.transform.position, -Vector3.up, out hit, hoverHeight + 0.5f, layerMask))
            {
                Debug.DrawRay(hoverPoint.transform.position, -Vector3.up * hoverHeight, Color.green, 0.5f, false);

                body.AddForceAtPosition(Vector3.up * ((hoverForce) * (1.0f - (hit.distance / hoverHeight))) * hoverDamping, hoverPoint.transform.position);

                if(Wheels[i] != null)
                    Wheels[i].transform.position = hit.point + Vector3.up * 1f;

                if (Wheels[i] != null)
                    Wheels[i].transform.up = Vector3.Slerp(Wheels[i].transform.up,hit.normal,0.5f);

                grounded = true;
            }
            else
            {
                if (Wheels[i] != null)
                    Wheels[i].transform.position = hoverPoint.transform.position - Vector3.up * (hoverHeight - 0.5f);

                Debug.DrawRay(hoverPoint.transform.position, -Vector3.up * hoverHeight, Color.red, 0.5f, false);

                // Self levelling - returns the vehicle to horizontal when not grounded
                //if (transform.position.y > hoverPoint.transform.position.y)
                //{
                //    body.AddForceAtPosition(hoverPoint.transform.up * gravityForce, hoverPoint.transform.position);
                //}
                //else
                //{
                //    body.AddForceAtPosition(hoverPoint.transform.up * -gravityForce, hoverPoint.transform.position);
                //}
            }
        }

        if (grounded)
        {
            body.drag = groundedDrag;
        }
        else
        {
            body.drag = 0.01f;
            thrust /= 10f;
            turnValue /= 10f;
        }

        // Handle Forward and Reverse forces
        if(grounded)
            body.AddForce((target - transform.position)*400);
        

        // Handle Turn forces
        if (turnValue > 0)
        {
            body.AddRelativeTorque(Vector3.up * turnValue * turnStrength);
            body.AddForceAtPosition((-rightPushPoint.transform.up * gravityForce * hoverDamping), rightPushPoint.transform.position);
        }
        else if (turnValue < 0)
        {
            body.AddRelativeTorque(Vector3.up * turnValue * turnStrength);
            body.AddForceAtPosition((-leftPushPoint.transform.up * gravityForce * hoverDamping), leftPushPoint.transform.position);
        }

        // Limit max velocity
        if (body.velocity.sqrMagnitude > (body.velocity.normalized * maxVelocity).sqrMagnitude)
        {
            body.velocity = body.velocity.normalized * maxVelocity;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(target, 8);
    }
}

public static class ExtensionMethods
{

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
