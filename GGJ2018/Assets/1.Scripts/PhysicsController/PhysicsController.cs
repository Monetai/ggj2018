﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    /// <summary>
    /// really basic movement controller
    /// </summary>
    public class PhysicsController : MonoBehaviour
    {
        public float hoverHeight;
        public float rotationSpeed;
        public float speed = 3f;
        private Rigidbody body;

        private Vector3 velocity = Vector3.zero;

        // Use this for initialization
        void Start()
        {
            body = GetComponent<Rigidbody>();
        }

        public void SetVelocity(Vector3 newVel)
        {
            velocity = newVel;
        }

        private void Update()
        {

        }

        private void FixedUpdate()
        {
            RaycastHit hit;
            if (Physics.Raycast(body.position, Vector3.down * (hoverHeight), out hit))
            {
                if (hit.collider.gameObject != this.gameObject)
                {
                    Vector3 pos = body.position;
                    pos.y = (hit.point + hit.normal.normalized * hoverHeight).y;
                    body.position = pos;
                }
            }

            body.AddForce(velocity * speed, ForceMode.VelocityChange);

            velocity = Vector3.zero;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            
        }

        public void RotateTo(Vector3 wordlSpaceRotation)
        {
            if (!enabled)
                return;

            float angle = Mathf.Atan2(wordlSpaceRotation.x, wordlSpaceRotation.z) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, angle, 0)), Time.deltaTime * rotationSpeed);
        }

        /// <summary>
        /// Return the angle between 0° and 360°
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        float NormaliseAngle(float angle)
        {
            angle %= 360;

            if (angle < 0)
            {
                angle += 360;
            }

            return angle;
        }

        public float GetAngleFromForward(Vector3 direction)
        {
            Vector3 axis = Vector3.Cross(transform.forward, direction);
            return Vector3.Angle(transform.forward, direction) * (axis.y < 0 ? -1 : 1);
        }
    }
}