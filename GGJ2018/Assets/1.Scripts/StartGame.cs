﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class StartGame : MonoBehaviour {

    public GameObject uiToActivate;
    bool keyDownAvailable = false;
    TitleShowFade titleShowHideFade;
    bool done = false;
    public GameObject timelineToPlay;

    private void Start()
    {
        StartCoroutine(WaitForInput());
        titleShowHideFade = GetComponent<TitleShowFade>();
    }

    // Update is called once per frame
    void Update () {
        if(done == false)
        {
            if (keyDownAvailable == true )
            {
                if (Input.anyKeyDown)
                {
                    AkSoundEngine.PostEvent("sfx_ui_validation", gameObject);
                    uiToActivate.SetActive(true);

                    titleShowHideFade.Hide();

                    timelineToPlay.SetActive(true);
                    timelineToPlay.GetComponent<PlayableDirector>().Play();

                    AkSoundEngine.SetState("music", "music_step_game");

                    StartCoroutine(WaitForLogo());
                }

            }
        }
    }

    IEnumerator WaitForInput()
    {
        yield return new WaitForSeconds(2);
        keyDownAvailable = true;
    }

    IEnumerator WaitForLogo()
    {
        yield return new WaitForSeconds(3);
        done = true;
        gameObject.SetActive(false);
    }
}
