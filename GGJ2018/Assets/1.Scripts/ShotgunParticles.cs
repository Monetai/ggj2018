﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShootGun))]
public class ShotgunParticles : MonoBehaviour {

    ShootGun gun;

    public GameObject objectToInstantiateOnShoot;
    public GameObject objectToInstantiateOnCantShoot;

    // Use this for initialization
    void Start ()
    {
        gun = GetComponent<ShootGun>();
        gun.OnCantShoot += OnCantShoot;
        gun.OnShoot += OnShoot;
    }

    void OnShoot()
    {
        AkSoundEngine.PostEvent("sfx_shotgun_shoot", objectToInstantiateOnShoot);
        InstantiateEffect(objectToInstantiateOnShoot);
    }

    void OnCantShoot()
    {
        AkSoundEngine.PostEvent("sfx_shotgun_cantshoot", objectToInstantiateOnCantShoot);
        InstantiateEffect(objectToInstantiateOnCantShoot);
    }

    private void InstantiateEffect(GameObject effectGameObject)
    {
        GameObject obj = Instantiate(effectGameObject);
        obj.transform.position = gun.transform.position;
        obj.transform.forward = gun.transform.forward;
    }
}
