﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using com.ootii.Messages;
namespace LittleWitch
{
    [CreateAssetMenu(fileName = "Event", menuName = "LittleWitch/Events/create new EventAsset", order = 2)]
    public class EventAsset : ScriptableObject
    {
        public string eventName;

        [Button("Sent Event")]
        private void SendEvent()
        {
            MessageDispatcher.SendMessage(eventName);
        }
    }
}
