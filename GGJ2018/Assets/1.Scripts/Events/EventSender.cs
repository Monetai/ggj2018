﻿using System.Collections.Generic;
using com.ootii.Messages;

namespace LittleWitch
{
    [System.Serializable]
    public class EventSender
    { 
        public List<EventAsset> eventsToSend;
            
        public void SendEvents()
        {
            foreach(EventAsset asset in eventsToSend)
            {
                MessageDispatcher.SendMessage(asset.eventName);
            }
        }
    }
}