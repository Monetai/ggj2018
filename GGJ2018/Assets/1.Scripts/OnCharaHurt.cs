﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LittleWitch;

public class OnCharaHurt : MonoBehaviour
{
    public StateController stateCtrl;
    private Life life;
    private void Start()
    {
        life = GetComponent<Life>();
        life.OnTakeDammage += OnTakeDammage;
    }

    void OnTakeDammage(Vector3 pos)
    {

        stateCtrl.SetDictionnaryValue("isHurt", "true");
    }
}
