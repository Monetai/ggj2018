﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace LittleWitch
{
    public class InputProvider : MonoBehaviour
    {
        public static InputProvider Instance;

        // Use this for initialization
        public void Awake()
        {
            DontDestroyOnLoad(this);
            Instance = this;
        }

        public bool GetAction(int action, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
                return ReInput.players.GetPlayer(playerIndex).GetButton(action);
            else
                return false;
        }

        public bool GetActionDown(int action, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
                return ReInput.players.GetPlayer(playerIndex).GetButtonDown(action);
            else
                return false;
        }

        public bool GetActionUp(int action, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
                return ReInput.players.GetPlayer(playerIndex).GetButtonUp(action);
            else
                return false;
        }

        public float GetAxis(int axis, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
                return ReInput.players.GetPlayer(playerIndex).GetAxis(axis);
            else
                return 0f;
        }

        public bool GetButtonRepeating(int action, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
                return ReInput.players.GetPlayer(playerIndex).GetButtonRepeating(action);
            else
                return false;
        }

        public void SetMapState(bool state, int category, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
                ReInput.players.GetPlayer(playerIndex).controllers.maps.SetMapsEnabled(state, category);
        }

        public void Vibrate(float force, float duration, int playerIndex)
        {
            if (ReInput.players.GetPlayer(playerIndex) != null)
            {
                ReInput.players.GetPlayer(playerIndex).SetVibration(0, force, duration);
                ReInput.players.GetPlayer(playerIndex).SetVibration(1, force, duration);
            }
        }

    }
}
