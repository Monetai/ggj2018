﻿using UnityEngine;
using System.Collections;
using System;

namespace LittleWitch
{
    public class NoInputSource : InputSource
    {
        public override float GetMoveHorizontal()
        {
            return 0;
        }

        public override float GetMoveVertical()
        {
            return 0;
        }

    }
}