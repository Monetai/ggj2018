﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace LittleWitch
{
    public class PlayerInputSource :  InputSource
    {
        //Left Stick
        public override float GetMoveHorizontal()
        {
            return InputProvider.Instance.GetAxis(RewiredConsts.Action.Default.MoveHorizontal, playerIndex);
        }

        public override float GetMoveVertical()
        {
            return InputProvider.Instance.GetAxis(RewiredConsts.Action.Default.MoveVertical, playerIndex);
        }


        //Interact Button
        public override bool GetInteractButton()
        {
            return InputProvider.Instance.GetAction(RewiredConsts.Action.Default.Interact, playerIndex);
        }

        public override bool GetInteractButtonDown()
        {
            return InputProvider.Instance.GetActionDown(RewiredConsts.Action.Default.Interact, playerIndex);
        }

        public override bool GetInteractButtonUp()
        {
            return InputProvider.Instance.GetActionUp(RewiredConsts.Action.Default.Interact, playerIndex);
        }

        //Fire Button
        public override bool GetFireButton()
        {
            return InputProvider.Instance.GetAction(RewiredConsts.Action.Default.Fire, playerIndex);
        }

        public override bool GetFireButtonDown()
        {
            return InputProvider.Instance.GetActionDown(RewiredConsts.Action.Default.Fire, playerIndex);
        }

        public override bool GetFireButtonUp()
        {
            return InputProvider.Instance.GetActionUp(RewiredConsts.Action.Default.Fire, playerIndex);
        }
    }
}
