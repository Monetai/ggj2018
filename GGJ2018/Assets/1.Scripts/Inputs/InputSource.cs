﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    /// <summary>
    /// Base class for each type of inputs to control an entity (IA script, player control, text-based control..)
    /// </summary>
    public abstract class InputSource : MonoBehaviour
    {
        protected int playerIndex;

        public void SetPlayerIndex(int index)
        {
            playerIndex = index;
        }

        public int GetPlayerIndex()
        {
            return playerIndex;
        }

        public virtual float GetMoveHorizontal(){return 0;}
        public virtual float GetMoveVertical(){return 0;}
        public virtual Vector2 GetMoveVector(){return new Vector2(GetMoveHorizontal(), GetMoveVertical());}

        public virtual bool GetShootButton() { return false; }
        public virtual bool GetShootButtonDown() { return false; }
        public virtual bool GetShootButtonUp() { return false; }

        public virtual bool GetInteractButton() { return false; }
        public virtual bool GetInteractButtonDown() { return false; }
        public virtual bool GetInteractButtonUp() { return false; }

        public virtual bool GetFireButton() { return false; }
        public virtual bool GetFireButtonDown() { return false; }
        public virtual bool GetFireButtonUp() { return false; }

        public virtual bool GetOpenMenuToolsButton() { return false; }
        public virtual bool GetOpenMenuToolsButtonDown() { return false; }
        public virtual bool GetOpenMenuToolsButtonUp() { return false; }

    }
}