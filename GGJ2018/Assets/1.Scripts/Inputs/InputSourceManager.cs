﻿using UnityEngine;
using System.Collections;

namespace LittleWitch
{
    /// <summary>
    /// Class to handle switching from different possible input source
    /// </summary>
    public class InputSourceManager : MonoBehaviour
    {
        public int playerIndex;
        InputSource currentSource;
        InputSource prevSource;

        // Use this for initialization
        void Awake()
        {
            currentSource = GetComponent<InputSource>();
            currentSource.SetPlayerIndex(playerIndex);

            if (!currentSource)
            {
                currentSource = gameObject.AddComponent<NoInputSource>();
                currentSource.SetPlayerIndex(playerIndex);
            }
        }

        public void SetNoInputs()
        {
            prevSource = currentSource;
            currentSource = gameObject.AddComponent<NoInputSource>();
            currentSource.SetPlayerIndex(playerIndex);
        }

        public void SetToPrevInput()
        {
            InputSource trans = currentSource;
            currentSource = prevSource;
            currentSource.SetPlayerIndex(playerIndex);

            prevSource = trans;
            prevSource.SetPlayerIndex(playerIndex);
        }

        public void SetInputSource(InputSource source)
        {
            prevSource = currentSource;
            prevSource.SetPlayerIndex(playerIndex);

            currentSource = source;
            currentSource.SetPlayerIndex(playerIndex);
        }

        public InputSource SetInputSource()
        {
            return currentSource;
        }

        public InputSource GetCurrentInputSource()
        {
            return currentSource;
        }

        public InputSource GetPreviousInputSource()
        {
            return prevSource;
        }
    }
}