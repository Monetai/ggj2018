﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class GameWinManager : MonoBehaviour {

    public List<GameObject> wheels;
    public List<GameObject> players;

    bool playerLose = false;
    bool playerWin = false;

    bool gameFinish = false;
    bool done = false;

    bool playersAllNull = true;
    bool wheelsAllNull = true;

    public Action onLose, onWin;

    public GameObject timelineToPlayEnd;

    // Update is called once per frame
    void Update()
    {
        if (gameFinish == false)
        {
            playersAllNull = true;
            wheelsAllNull = true;

            if (players.Count != 0)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    if (players[i] != null)
                        playersAllNull = false;
                }
                if (playersAllNull == true)
                {
                    gameFinish = true;
                    playerLose = true;
                }
            }

            if (wheels.Count != 0)
            {
                for (int i = 0; i < wheels.Count; i++)
                {
                    if (wheels[i] != null)
                        wheelsAllNull = false;
                }
                if (wheelsAllNull == true)
                {
                    gameFinish = true;
                    playerWin = true;
                }
            }
        }

        if(gameFinish == true && done == false)
        {
            AkSoundEngine.SetState("music", "music_step_end");
            if(playerWin == true)
            {
                Debug.Log("win");
                if (onWin != null)
                {
                    onWin.Invoke();
                }
                done = true;
            }
            if (playerLose == true)
            {
                Debug.Log("lose");
                if (onLose != null)
                {
                    onLose.Invoke();
                }
                done = true;
            }

            timelineToPlayEnd.SetActive(true);
            timelineToPlayEnd.GetComponent<PlayableDirector>().Play();

            StartCoroutine(WaitToReload());
        }
    }

    IEnumerator WaitToReload()
    {
        yield return new WaitForSeconds(10);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
