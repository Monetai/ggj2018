﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BigShootPattern : BossPattern
{
    public GameObject chargeParticule;
    public GameObject bulletPrefab;
    public GameObject canon;

    GameObject target;
    bool haveShoot;
    bool followTarget = true;

    public override void InitPattern()
    {
        GameObject[] characters = GameObject.FindGameObjectsWithTag("Player");
        target = characters[Random.Range(0, characters.Length - 1)];
        haveShoot = false;
        followTarget = true;

        StartCoroutine(PrepareShoot());
    }

    public override void UpdatePattern()
    {
        if (followTarget && target)
        {
            canon.transform.LookAt(target.transform);
            Vector3 rot = canon.transform.rotation.eulerAngles;
            rot.z = 0;
            rot.x = 0;
            canon.transform.eulerAngles = rot;
        }
    }

    IEnumerator PrepareShoot()
    {
        yield return new WaitForSeconds(6f);
        AkSoundEngine.PostEvent("sfx_nmi_charge", gameObject);
        chargeParticule.transform.DOScale(4, 4.5f);
        yield return new WaitForSeconds(5f);
        chargeParticule.transform.DOScale(0, 0.2f);
        followTarget = false;
        yield return new WaitForSeconds(0.4f);
        AkSoundEngine.PostEvent("sfx_nmi_shoot", gameObject); 
        GameObject obj  = Instantiate(bulletPrefab);
        obj.transform.position = chargeParticule.transform.position;
        obj.transform.forward = chargeParticule.transform.forward;
        haveShoot = true;

    }

    public override void EndPattern()
    {
        haveShoot = false;
        followTarget = true;
        StopAllCoroutines();
    }

    public override bool CanEndPattern()
    {
        return haveShoot;
    }

    private void OnDrawGizmos()
    {
    }
}
