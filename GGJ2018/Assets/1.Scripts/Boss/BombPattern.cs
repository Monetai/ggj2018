﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombPattern : BossPattern {

    public GameObject prefabToInstantiate;
    public GameObject center;

    public float radius = 50;
    public float bombAmount = 50;

    public override void InitPattern()
    {
        for(int i = 0; i < bombAmount; i++)
        {
            Vector2 randPos = UnityEngine.Random.insideUnitCircle*radius;
            GameObject obj =  Instantiate(prefabToInstantiate);
            obj.transform.position = center.transform.position + new Vector3(randPos.x,0,randPos.y);
        }
    }

    public override void UpdatePattern()
    {

    }

    public override void EndPattern()
    {

    }

    public override bool CanEndPattern()
    {
        return true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(center.transform.position,radius);
    }
}
