﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BossPattern : MonoBehaviour {

    public abstract void InitPattern();
    public abstract void UpdatePattern();
    public abstract void EndPattern();
    public abstract bool CanEndPattern();

}
