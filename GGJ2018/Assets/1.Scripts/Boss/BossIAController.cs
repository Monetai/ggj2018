﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIAController : MonoBehaviour {

    public List<BossPattern> avaiablePatterns;

    public List<GameObject> targetCharacters;

    private float target;

    BossPattern activePattern;

    public GameObject center;
    public float radius = 20;
    Vector3 targetPos;

    BossPhysicController ctrl;

    // Use this for initialization
    void Start ()
    {
        ctrl = GetComponent<BossPhysicController>();
        activePattern = avaiablePatterns[0];
        activePattern.InitPattern();

        targetPos = GetNewPos();
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 normPos = ctrl.transform.position;
        normPos.y = targetPos.y;

        if (activePattern.CanEndPattern())
        {
            activePattern.EndPattern();
            activePattern.InitPattern();
        }
        else
        {
            activePattern.UpdatePattern();
        }

        if (Vector3.Dot(ctrl.transform.right, targetPos) > 0.15f)
        {
            ctrl.turnAxis = Mathf.Lerp(0.2f, 1f, Mathf.Abs(Vector3.Dot(ctrl.transform.right, targetPos)));
        }
        else if (Vector3.Dot(ctrl.transform.right, targetPos) < 0.15f)
        {
            ctrl.turnAxis = -Mathf.Lerp(0.2f, 1f, Mathf.Abs(Vector3.Dot(ctrl.transform.right, targetPos)));
        }


        ctrl.target = targetPos;
        

        //if (Mathf.Abs(Vector3.Angle(ctrl.transform.forward, targetPos - normPos)) < 30)
        //{
        //    ctrl.acceleration = 0.5f;
        //}
        //else
        //{
        //    ctrl.acceleration = 0;
        //}

        if ( Vector3.Distance(targetPos, normPos) < 5)
        {
            targetPos = GetNewPos();
        }
    }

    public Vector3 GetNewPos()
    {
        Vector2 randPos = Random.insideUnitCircle * radius;
        return new Vector3(randPos.x, 0, randPos.y);
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawSphere(targetPos, 5);
    }
}
