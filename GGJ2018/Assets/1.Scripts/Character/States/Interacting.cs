﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
namespace LittleWitch
{
    public class Interacting : State
    {
        [BoxGroup("Mandatory References")]
        public InteractionManager interactManager;
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;
        
        protected override void Awake()
        {
            base.Awake();
        }

        // Use this for initialization
        protected override void Start()
        {
            if (interactManager == null)
                Destroy(this);
        }

        public override bool CanBeActive()
        {
            if (inputManager.GetCurrentInputSource().GetInteractButtonDown() && interactManager.CanInteract())
            {
                return true;
            }
            return false;
        }

        public override void OnBecameCurrentState(State previousState)
        {
            base.OnBecameCurrentState(previousState);
            interactManager.PrepareInteraction();
            if (interactManager.GetCurrentInteraction() != null && interactManager.GetCurrentInteraction().interactionBrain != null)
            {
                if(previousState is Move)
                {
                    Move state = previousState as Move;
                    state.StopMoving();
                }
                inputManager.SetNoInputs();
            }
        }

        public override bool CanBeSkipped()
        {
            if((interactManager.GetCurrentInteraction() != null && interactManager.GetCurrentInteraction().interactionBrain != null) && interactManager.IsInteracting())
                return false;
            else
                return true; 
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            if(inputManager.GetCurrentInputSource() is NoInputSource)
                inputManager.SetToPrevInput();
        }
    }
}
