﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class Move : State
    {
        [BoxGroup("Mandatory References")]
        public Animator animator;
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;
        [BoxGroup("Mandatory References")]
        public PhysicsController physicsController;

        private int speedId = 0;
        private int directionId = 0;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
        }

        #region StateCallbacks
        public override bool CanBeActive()
        {
            if (new Vector3(inputManager.GetCurrentInputSource().GetMoveHorizontal(), 0, inputManager.GetCurrentInputSource().GetMoveVertical()).magnitude > 0.1f)
                return true;
            else
                return false;
        }

        public override void OnUpdateState()
        {
            base.OnUpdateState();

            Vector3 stickDirection = new Vector3(inputManager.GetCurrentInputSource().GetMoveHorizontal(), 0, inputManager.GetCurrentInputSource().GetMoveVertical());

            if (stickDirection.magnitude > 1)
                stickDirection.Normalize();

            Vector3 speedVector = stickDirection;

            float direction = physicsController.GetAngleFromForward(stickDirection);

            //if (!inTurn)
            //{
            physicsController.RotateTo(stickDirection);
            physicsController.SetVelocity(speedVector);
            //}
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            animator.SetBool("isMoving", false);
        }

        public void StopMoving()
        {
        }

        public override void OnBecameCurrentState(State previousState)
        {
            base.OnBecameCurrentState(previousState);
            animator.SetBool("isMoving", true);

        }
        #endregion


    }
}