﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class Fire : State
    {
        [BoxGroup("Mandatory References")]
        public Animator animator;
        [BoxGroup("Mandatory References")]
        public InputSourceManager inputManager;
        [BoxGroup("Mandatory References")]
        public ShootGun shootGun;

        private int speedId = 0;

        protected override void Awake()
        {
            base.Awake();
        }

        #region StateCallbacks
        public override bool CanBeActive()
        {
            if (inputManager.GetCurrentInputSource().GetFireButtonDown())
            {
                if (shootGun.IsReadyToShoot())
                {
                    return true;
                }
                else 
                {
                    shootGun.Shoot();
                    return false;
                }
            }
            else
                return false;
        }

        public override void OnUpdateState()
        {
            base.OnUpdateState();
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
            animator.SetBool("isShooting", false);

        }

        public override bool CanBeSkipped()
        {
            return waitForEnd == false;
        }

        bool waitForEnd;

        public override void OnBecameCurrentState(State previousState)
        {
            base.OnBecameCurrentState(previousState);
           
            animator.SetBool("isShooting", true);
            
            StartCoroutine(Wait());
        }
        
        IEnumerator Wait()
        {
            waitForEnd = true;
            yield return new WaitForSeconds(0.8f);
            shootGun.Shoot();
            InputProvider.Instance.Vibrate(0.4f, 0.3f, inputManager.GetCurrentInputSource().GetPlayerIndex());
            yield return new WaitForSeconds(1f);
            waitForEnd = false;
        }

        #endregion
    }
}