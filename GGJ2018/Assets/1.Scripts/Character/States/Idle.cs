﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace LittleWitch
{
    public class Idle : State
    {
        [BoxGroup("Mandatory References")]
        public Animator animator;

        private int speedId = 0;

        protected override void Awake()
        {
            base.Awake();
            speedId = Animator.StringToHash("Speed");
        }

        #region StateCallbacks
        public override bool CanBeActive()
        {
            return true;
        }

        public override void OnUpdateState()
        {
            base.OnUpdateState();
        }

        public override void OnStateEnded()
        {
            base.OnStateEnded();
        }

        public override void OnBecameCurrentState(State previousState)
        {
            //animator.SetFloat(speedId, 0);
            base.OnBecameCurrentState(previousState);
        }
        #endregion
    }
}