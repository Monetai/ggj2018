﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidAnimation : MonoBehaviour
{
    [Range(0, 1)]
    public float liquidLevel = 0.5f;
    [Range(0.1f, 2)]
    public float pendulumLenght = 2.0f;
    [Range(0, 1)]
    public float damping;

    public Vector3 gravityDirection = Vector3.down;
    public bool debugPendulum = false;
    public float debugSize = 0.5f;

    private Material mat;
    private Vector3 pendulum;
    private Vector3 pendulumVelocity;
    private Vector2 liquidBounds;

    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private Mesh mesh;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.sharedMesh;
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        pendulum -= gravityDirection.normalized * -1 * Time.deltaTime * (1.0f - damping);
        pendulum.Normalize();
        CalculateMeshBound();
        mat = meshRenderer.material;
        mat.SetFloat("_SurfaceHeight", Mathf.Lerp(liquidBounds.x, liquidBounds.y, liquidLevel));
    }

    private void CalculateMeshBound()
    {
        Vector3[] verts = mesh.vertices;
        liquidBounds.x = liquidBounds.y = (transform.TransformPoint(verts[0]) - transform.position).y;
        for (int i = 0; i < verts.Length; ++i)
        {
            Vector3 worldPosition = transform.TransformPoint(verts[i]) - transform.position;
            if (worldPosition.y > liquidBounds.y)
            {
                liquidBounds.y = worldPosition.y;
            }
            if (worldPosition.y < liquidBounds.x)
            {
                liquidBounds.x = worldPosition.y;
            }
        }
    }

    void FixedUpdate()
    {
        CalculatePendulumVelocity();

        CalculateMeshBound();

        mat.SetFloat("_SurfaceHeight", Mathf.Lerp(liquidBounds.x - Single.Epsilon * 10, liquidBounds.y + Single.Epsilon * 10, liquidLevel));
        mat.SetVector("_Pendulum", transform.InverseTransformPoint(pendulum));
        mat.SetVector("_SurfacePoint", transform.InverseTransformPoint(transform.position - (pendulum - transform.position).normalized * Mathf.Lerp(liquidBounds.x, liquidBounds.y, liquidLevel)));
    }

    void CalculatePendulumVelocity()
    {
        //move toward the displacement
        pendulum += pendulumVelocity * damping;
        Vector3 prevPos = pendulum;
        //pull toward the gravity vector
        pendulum -= -gravityDirection.normalized * Time.deltaTime * damping;

        if (Vector3.Distance(pendulum, transform.position) > pendulumLenght)
        {
            pendulum = transform.position + (pendulum - transform.position).normalized * pendulumLenght;
        }
        pendulumVelocity += (pendulum - prevPos);
    }

    void OnDrawGizmosSelected()
    {
        if (debugPendulum)
        {
            Vector3 anchorDirection = transform.position - transform.TransformDirection(Vector3.up) * pendulumLenght;
            if (pendulum == Vector3.zero)
                pendulum = anchorDirection;

            //anchor absolute position
            Gizmos.DrawSphere(anchorDirection, debugSize * 0.1f);
            Gizmos.DrawLine(transform.position, anchorDirection);

            Gizmos.color = Color.blue;
            //the ocation of the anchor (physics)
            Gizmos.DrawSphere(pendulum, debugSize * 0.1f);
        }
    }
}
