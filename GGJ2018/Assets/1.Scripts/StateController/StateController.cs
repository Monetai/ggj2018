﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace LittleWitch
{
    public class StateController : MonoBehaviour
    {

        private List<State> states;
        public List<State> States
        {
            get { return states; }
            // set { states = value; }
        }

        private State currentState;
        public State CurrentState
        {
            get { return currentState; }
            set
            {
                SetCurrentState(currentState);
            }
        }

        /// <summary>
        /// A dictionnary that allow the states to communicate
        /// with each other
        /// </summary>
        public Dictionary<string, string> stateDictionnary = new Dictionary<string, string>();

        #region Callbacks

        public System.Action<State> OnCurrentStateChanged;

        #endregion

        #region UnityFunctions

        void Awake()
        {
            Init();
        }

        void Update()
        {

            SearchForCurrentState();

            if (currentState != null)
                currentState.OnUpdateState();

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        void OnDisable()
        {
            if (currentState != null)
            {
                SetCurrentState(null);
            }
        }

        #endregion

        #region StateManagement

        /// <summary>
        /// Searchs for the best current State.
        /// If the State is different than the current one, the SetCurrentState method
        /// is called.
        /// </summary>
        private void SearchForCurrentState()
        {
            // First off, if our current State cant be skiped, no point to continue.
            if (currentState != null && !currentState.CanBeSkipped())
                return;

            State candidate = GetBestCandidateForCurrentState();

            // If its not already the current State, set it as curent.
            if (candidate != currentState)
            {
                SetCurrentState(candidate);
            }
        }

        /// <summary>
        /// Sets the current State and call all the callbacks needed. (OnStateEnded, 
        /// OnBecomeCurrentState, etc.)
        /// </summary>
        /// <param name="a">The alpha component.</param>
        private void SetCurrentState(State newCurrent)
        {
            // Just in case
            if (currentState == newCurrent)
                return;

            State previousState = currentState;

            if (currentState != null)
                currentState.OnStateEnded();

            currentState = newCurrent;

            if (currentState != null)
            {
                currentState.OnBecameCurrentState(previousState);

                if (OnCurrentStateChanged != null)
                    OnCurrentStateChanged(currentState);
            }
            else
            {
                Debug.Log("StateController : " + name + " have no longer a current State. Idling. (previousState : " + previousState.GetType() + ")");
            }
        }

        private void UpdateCurrentState()
        {
            currentState.OnUpdateState();
        }

        /// <summary>
        /// Called when an State have its priority changed.
        /// We need to update the order of our State list.
        /// </summary>
        /// <param name="a">The State that has its priority changed.</param>
        public void OnStatePriorityChanged(State a)
        {
            SortStatesByPriority();
        }

        #endregion

        #region ControllerManagement

        public void SortStatesByPriority()
        {
            states.Sort((x, y) => y.Priority.CompareTo(x.Priority));
        }

        #endregion

        #region Initialization

        public void Init()
        {
            FetchStates();

            // If no States were found, just stop
            if (states == null || states.Count <= 0)
                return;

            SortStatesByPriority();
            NotifyStates();
        }

        /// <summary>
        /// Fetchs the States attached to our game object and set up the State list.
        /// </summary>
        private void FetchStates()
        {
            State[] localStates = GetComponents<State>();

            if (localStates == null || localStates.Length <= 0)
            {
                Debug.LogWarning("StateController : " + name + " have no States attached.");
                //enabled = false;
                return;
            }

            states = new List<State>(localStates);
        }

        /// <summary>
        /// Notify our attached States that we are their controller.
        /// </summary>
        private void NotifyStates()
        {
            // Set the controller of each State
            states.ForEach(x => x.Controller = this);
        }

        #endregion

        #region Helpers

        public State GetBestCandidateForCurrentState()
        {
            // Lets find the first State available (States is sorted by priority at this point)
            for (int i = 0; i < States.Count; i++)
            {
                State candidate = States[i];

                // If our candidate is null (have been destroyed for instance)
                if (candidate == null)
                    continue;

                // If our candidate is not enabled we just skip it
                if (!candidate.IsEnabled)
                    continue;

                // If our candidate is ready
                if (candidate.CanBeActive())
                {
                    return candidate;
                }
            }

            return null;
        }

        public State GetStateByID(string id)
        {
            if (states == null || states.Count <= 0)
                return null;

            return states.Find(x => x.id == id);
        }

        public List<State> GetStatesByType(System.Type t)
        {
            if (states == null || states.Count <= 0)
                return null;

            return states.FindAll(x => x.GetType() == t);
        }

        public T GetState<T>() where T : State
        {
            if (states == null || states.Count <= 0)
                return null;

            List<State> candidates = states.FindAll(x => x is T);

            if (candidates.Count <= 0)
                return null;

            return candidates[0] as T;
        }

        public State GetStateByType(System.Type t)
        {
            if (states == null || states.Count <= 0)
                return null;

            List<State> candidates = states.FindAll(x => x.GetType() == t);

            if (candidates.Count <= 0)
                return null;

            return candidates[0];
        }

        public List<State> GetStatesWithHigherPriority(int priority)
        {
            if (states == null || states.Count <= 0)
                return null;

            return states.FindAll(x => x.Priority > priority);
        }

        public List<State> GetStatesWithHigherPriority(State a)
        {
            return GetStatesWithHigherPriority(a.Priority);
        }

        public void SetDictionnaryValue(string id, string val)
        {
            if (!stateDictionnary.ContainsKey(id))
            {
                stateDictionnary.Add(id, val);
            }
            else
            {
                stateDictionnary[id] = val;
            }
        }

        public string GetDictionnaryValue(string id, string defaultValue = "")
        {
            if (stateDictionnary.ContainsKey(id))
                return stateDictionnary[id];

            return defaultValue;
        }

        #endregion
    }

}