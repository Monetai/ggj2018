﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;


namespace LittleWitch
{
    public class WwiseWrapper : MonoBehaviour
    {
        public GameObject listener;
        public static WwiseWrapper Instance;
        Dictionary<string, WIFEvents> events = new Dictionary<string, WIFEvents>();

        public bool fixdemerdepourlesondemaxime = false;

        private void Awake()
        {

            Instance = this;
            Object[] eventAssets;
            WwiseImporterSettings settings = null;
            // load the wifsettings file
            Object[] settingsObj = Resources.LoadAll("", typeof(WwiseImporterSettings));
            if(settingsObj.Length == 0)
            {
                Debug.LogError("Cannot initialise WIFWrapper!!");
                settings = ScriptableObject.CreateInstance<WwiseImporterSettings>();
            }
            else
            {
                settings = settingsObj[0] as WwiseImporterSettings;
            }
            // take the path from resources !warning about Resources/ and change regex
            string[] pathToLoad = Regex.Split(settings.dirWIFEvents, "Resources/");

            // load all wifevents
            eventAssets = Resources.LoadAll(pathToLoad[1], typeof(WIFEvents));
            foreach (Object evt in eventAssets)
            {
                WIFEvents evet = evt as WIFEvents;
                events.Add(evet.eventName, evet);
            }
        }

        //----------------------------------------------------------------------------------
        // EVENT : play sound with string
        public static void WIFPlayEvent(string evt, GameObject gameObj)
        {
            WIFEvents wIFEvent = null;
            Instance.events.TryGetValue(evt,out wIFEvent);
            if (wIFEvent != null)
                AkSoundEngine.PostEvent(wIFEvent.eventName, gameObj);
            else
                Debug.LogError(evt + " not found");
        }

        //----------------------------------------------------------------------------------
        // EVENT : play sound with wif
        public static void WIFPlayEvent(WIFEvents evt, GameObject gameObj)
        {
            if (evt)
                AkSoundEngine.PostEvent(evt.eventName, gameObj);
            else
                Debug.LogError(evt + "null");
        }

        //----------------------------------------------------------------------------------
        // BANK : load soundbank
        public static void WIFLoadBank(string bank)
        {
            uint bankID;

            AKRESULT result = AkSoundEngine.LoadBank(bank, AkSoundEngine.AK_DEFAULT_POOL_ID, out bankID);

            Debug.Log("WIF Load Bank \"" + bank + "\" (Result = " + result + ")");
        }

        //----------------------------------------------------------------------------------
        // BANK : load soundbank list
        public static void WIFLoadBank(List<string> banks)
        {
            uint bankID;

            foreach(string bnk in banks)
            {
                AKRESULT result = AkSoundEngine.LoadBank(bnk, AkSoundEngine.AK_DEFAULT_POOL_ID, out bankID);

                Debug.Log("WIF Load Bank \"" + bnk + "\" (Result = " + result + ")");
            }
        }

        //----------------------------------------------------------------------------------
        // SWITCH : set switch on wifevent with uint
        public static void WIFSetSwitch(uint switchGroup, uint switchState, GameObject gameObj)
        {
            AkSoundEngine.SetSwitch(switchGroup, switchState, gameObj);
        }

        //----------------------------------------------------------------------------------
        // SWITCH : set switch on wifevent with string
        public static void WIFSetSwitch(string switchGroup, string switchState, GameObject gameObj)
        {
            AkSoundEngine.SetSwitch(switchGroup, switchState, gameObj);
        }
    }
}