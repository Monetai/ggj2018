namespace LittleWitch {

	public static class WwiseConstsInfos {
		public const string pathToWIFEvents = "Assets/4.Sound/Resources/WIFEvents";
	}

	public static class WIFConstsEvents {
		public const string AMB_DEFAULT = "AMB_DEFAULT";
		public const string ENV_DEFAULT = "ENV_DEFAULT";
		public const string MUSIC_DEFAULT = "MUSIC_DEFAULT";
		public const string SFX_DEFAULT = "SFX_DEFAULT";
		public const string VO_DEFAULT_2D = "VO_DEFAULT_2D";
		public const string VO_DEFAULT_3D = "VO_DEFAULT_3D";
	}

	public static class WwiseConstsBanks {
		public const string INIT = "INIT";
		public const string SHARED = "SHARED";
	}

	public static class WwiseConstsBusses {
		public const string AMB = "AMB";
		public const string MASTER_AUDIO_BUS = "MASTER_AUDIO_BUS";
		public const string SFX = "SFX";
		public const string VO = "VO";
	}

	public static class WwiseConstsRTPC {
	}

	public static class WwiseConstsTriggers {
	}

	public static class WwiseConstsSwitches {
		public struct MATERIAL {
			public const string SWITCHGROUPNAME = "MATERIAL";
			public const uint SWITCHGROUPID = 3865314626;
			public const uint GRAVEL = 2185786256;
			public const uint GROUND = 2528658256;
			public const uint METAL = 2473969246;
			public const uint WOOD = 2058049674;
		}
	}

	public static class WwiseConstsStates {
		public struct MUSIC {
			public const string STATEGROUPNAME = "MUSIC";
			public const uint STATEGROUPID = 3991942870;
			public const uint STEP01 = 1062722166;
		}
	}

}
