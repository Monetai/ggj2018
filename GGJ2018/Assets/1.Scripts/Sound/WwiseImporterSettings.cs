﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class WwiseImporterSettings : ScriptableObject
    {
        public string fileWwiseIds = "Assets/StreamingAssets/Audio/GeneratedSoundBanks/Wwise_IDs.h";
        public string dirWwiseImporterSettings = "Assets/DEFAULTWIF";
        public string dirWwiseConsts = "Assets/DEFAULTWIF";
        public string dirWIFEvents = "Assets/DEFAULTWIF";
    }
}
