﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleWitch
{
    public class WwiseVolSwitchState : MonoBehaviour
    {
        public GameObject gameObj;
        public string switchGroup, switchState;

        public void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                WwiseWrapper.WIFSetSwitch(switchGroup, switchState, gameObj);
            }
        }
    }
}