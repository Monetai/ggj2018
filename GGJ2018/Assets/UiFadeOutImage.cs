﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UiFadeOutImage : MonoBehaviour {

    private void Start()
    {
        Color col = GetComponent<Image>().color;
        col.a = 0;
        GetComponent<Image>().color = col;
        GetComponent<Image>().DOFade(1,4.0f);
    }
}
