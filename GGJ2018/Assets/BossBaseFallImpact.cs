﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBaseFallImpact : MonoBehaviour {

    public GameObject boss;
	// Use this for initialization
	void Start () {
        AkSoundEngine.PostEvent("sfx_nmi_fall_impact", boss);
	}
}
