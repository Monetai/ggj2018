﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DammageInflicter : MonoBehaviour
{
    public int dammageAmount = 2;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Life>() != null)
            other.GetComponent<Life>().TakeDammage(dammageAmount, other.transform.position);
    }
}
